# vercmp
[![Go Reference](https://pkg.go.dev/badge/go.elara.ws/vercmp.svg)](https://pkg.go.dev/go.elara.ws/vercmp)

This is a simple library that compares two versions using an algorithm loosely based on the `rpmvercmp` algorithm.